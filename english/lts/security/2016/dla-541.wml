<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there was a password policy issue in libvirt, a
library for interfacing with different virtualization systems.</p>

<p>Setting an empty graphics password is documented as a way to disable
VNC/SPICE access, but QEMU does not always behave like that. VNC would
happily accept the empty password. We enforce the behavior by setting
password expiration to <q>now</q>.</p>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in libvirt version
0.9.12.3-1+deb7u2.</p>

<p>We recommend that you upgrade your libvirt packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-541.data"
# $Id: $
