<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in the Xen hypervisor. The
Common Vulnerabilities and Exposures project identifies the following
problems:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10013">CVE-2016-10013</a> (xsa-204)

  <p>Xen mishandles SYSCALL singlestep during emulation which can lead to
  privilege escalation. The vulnerability is only exposed to 64-bit x86
  HVM guests.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10024">CVE-2016-10024</a> (xsa-202)

  <p>PV guests may be able to mask interrupts causing a Denial of Service.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.6.lts1-5.</p>

<p>We recommend that you upgrade your xen packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-783.data"
# $Id: $
