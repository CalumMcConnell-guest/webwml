<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security issues have been found in the Mozilla Firefox web
browser: Multiple memory safety errors, use-after-frees, buffer overflows
and other implementation errors may lead to the execution of arbitrary
code, denial of service or domain spoofing.</p>

<p>Debian follows the extended support releases (ESR) of Firefox. Support
for the 45.x series has ended, so starting with this update we're now
following the 52.x releases.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
52.2.0esr-1~deb7u1.</p>

<p>We recommend that you upgrade your firefox-esr packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-991.data"
# $Id: $
