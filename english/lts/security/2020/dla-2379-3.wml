<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>The update of mediawiki released as DLA-2379-2 contained a defect in the
patch for <a href="https://security-tracker.debian.org/tracker/CVE-2020-25827">CVE-2020-25827</a> which resulted from a possible use of an
uninitialized variable.  Updated mediawiki packages are now available to
correct this issue.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
1:1.27.7-1~deb9u6.</p>

<p>We recommend that you upgrade your mediawiki packages.</p>

<p>For the detailed security status of mediawiki please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mediawiki">https://security-tracker.debian.org/tracker/mediawiki</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2379-3.data"
# $Id: $
