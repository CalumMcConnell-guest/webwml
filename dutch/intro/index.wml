#use wml::debian::template title="Inleiding tot Debian" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="93f9b3008408920efa7a3791fb329de4c58aac6c"

<a id=community></a>
<h2>Debian is een gemeenschap van mensen</h2>
<p>Duizenden mensen van over de hele wereld werken samen en zetten daarbij Vrije Software en de noden van gebruikers op de eerste plaats.</p>

<ul>
  <li>
    <a href="people">Mensen:</a>
    Wie we zijn, wat we doen
  </li>
  <li>
    <a href="philosophy">Filosofie:</a>
    Waarom we het doen en hoe we het doen
  </li>
  <li>
    <a href="../devel/join/">Meewerken:</a>
    U kunt hier deel van uitmaken!
  </li>
  <li>
    <a href="help">Hoe kunt u Debian helpen?</a>
  </li>
  <li>
    <a href="../social_contract">Sociaal Contract:</a>
    Onze morele agenda
  </li>
  <li>
    <a href="diversity">Diversiteitsverklaring</a>
  </li>
  <li>
    <a href="../code_of_conduct">Gedragscode</a>
  </li>
  <li>
    <a href="../partners/">Partners:</a>
    Bedrijven en organisaties die het Debian-project doorlopend ondersteunen
  </li>
  <li>
    <a href="../donations">Giften</a>
  </li>
  <li>
    <a href="../legal/">Juridische info </a>
  </li>
  <li>
    <a href="../legal/privacy">Gegevensbescherming</a>
  </li>
  <li>
    <a href="../contact">Ons contacteren</a>
  </li>
</ul>

<hr>

<a id=software></a>
<h2>Debian is een vrij besturingssysteem</h2>
<p>We starten met Linux en voegen er verschillende duizenden toepassingen aan toe om tegemoet te komen aan de behoeften van de gebruikers.</p>

<ul>
  <li>
    <a href="../distrib">Downloaden:</a>
    Andere varianten van Debian-images
  </li>
  <li>
  <a href="why_debian">Waarom Debian?</a>
  </li>
  <li>
    <a href="../support">Ondersteuning:</a>
    Getting help
  </li>
  <li>
    <a href="../security">Beveiliging:</a>
    Laatste update <br>
    <:{ $MYLIST = get_recent_list('security/1m', '1', '$(ENGLISHDIR)', 'bydate', '(2000\d+\w+|dsa-\d+)');
        @MYLIST = split(/\n/, $MYLIST);
        $MYLIST[0] =~ s#security#../security#;
        print $MYLIST[0]; }:>
  </li>
  <li>
    <a href="../distrib/packages"> Softwarepakketten:</a>
    Zoeken en bladeren in de lange lijst met onze software
  </li>
  <li>
    <a href="../doc"> Documentatie</a>
  </li>
  <li>
    <a href="https://wiki.debian.org"> Debian wiki</a>
  </li>
  <li>
    <a href="../Bugs"> Bugrapporten</a>
  </li>
  <li>
    <a href="https://lists.debian.org/">
    Mailinglijsten</a>
  </li>
  <li>
    <a href="../blends"> Specifieke uitgaven (pure blends):</a>
    Metapakketten die aan specifieke behoeften beantwoorden
  </li>
  <li>
    <a href="../devel"> Ontwikkelaarshoek:</a>
    Informatie die voornamelijk voor de ontwikkelaars van Debian van belang is
  </li>
  <li>
    <a href="../ports"> Ports/Architecturen:</a>
    CPU-architecturen die door ons ondersteund worden
  </li>
  <li>
    <a href="search">Informatie over het gebruik van de Debian-zoekmachine</a>.
  </li>
  <li>
    <a href="cn">Informatie over pagina's die in meerdere talen beschikbaar zijn</a>.
  </li>
</ul>

