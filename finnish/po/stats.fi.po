msgid ""
msgstr ""
"Project-Id-Version: debian-webwml\n"
"PO-Revision-Date: 2016-05-16 11:30+0300\n"
"Last-Translator: Tommi Vainikainen <tvainika@debian.org>\n"
"Language-Team: Finnish <debian-l10n-finnish@lists.debian.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/template/debian/stats_tags.wml:6
msgid "Debian web site translation statistics"
msgstr "Debianin www-sivuston käännöstilastot"

#: ../../english/template/debian/stats_tags.wml:10
msgid "There are %d pages to translate."
msgstr "Yhteensä %d käännettävää sivua."

#: ../../english/template/debian/stats_tags.wml:14
msgid "There are %d bytes to translate."
msgstr "Yhteensä %d tavua käännettävänä."

#: ../../english/template/debian/stats_tags.wml:18
msgid "There are %d strings to translate."
msgstr "Yhteensä %d merkkijonoa käännettävänä."

#: ../../stattrans.pl:282 ../../stattrans.pl:498
msgid "Wrong translation version"
msgstr "Väärä käännösversio"

#: ../../stattrans.pl:284
msgid "This translation is too out of date"
msgstr "Tämä käännös on vanhentunut"

#: ../../stattrans.pl:286
msgid "The original is newer than this translation"
msgstr "Alkuperäinen on uudempi kuin tämä käännös"

#: ../../stattrans.pl:290 ../../stattrans.pl:498
msgid "The original no longer exists"
msgstr "Alkuperäistä sivua ei enää ole olemassa"

#: ../../stattrans.pl:474
msgid "hit count N/A"
msgstr "osumalukua ei saatavilla"

#: ../../stattrans.pl:474
msgid "hits"
msgstr "osumaa"

#: ../../stattrans.pl:492 ../../stattrans.pl:493
msgid "Click to fetch diffstat data"
msgstr "Napsauta noutaaksesi muutostilastojen datan"

#: ../../stattrans.pl:603 ../../stattrans.pl:743
msgid "Created with <transstatslink>"
msgstr ""

#: ../../stattrans.pl:608
msgid "Translation summary for"
msgstr "Käännösyhteenveto"

#: ../../stattrans.pl:611 ../../stattrans.pl:767 ../../stattrans.pl:813
#: ../../stattrans.pl:856
msgid "Not translated"
msgstr "Ei käännetty"

#: ../../stattrans.pl:611 ../../stattrans.pl:766 ../../stattrans.pl:812
msgid "Outdated"
msgstr "Vanhentunut"

#: ../../stattrans.pl:611
msgid "Translated"
msgstr "Käännetty"

#: ../../stattrans.pl:611 ../../stattrans.pl:691 ../../stattrans.pl:765
#: ../../stattrans.pl:811 ../../stattrans.pl:854
msgid "Up to date"
msgstr "Ajan tasalla"

#: ../../stattrans.pl:612 ../../stattrans.pl:613 ../../stattrans.pl:614
#: ../../stattrans.pl:615
msgid "files"
msgstr "tiedostoa"

#: ../../stattrans.pl:618 ../../stattrans.pl:619 ../../stattrans.pl:620
#: ../../stattrans.pl:621
msgid "bytes"
msgstr "tavua"

#: ../../stattrans.pl:628
msgid ""
"Note: the lists of pages are sorted by popularity. Hover over the page name "
"to see the number of hits."
msgstr ""
"Huom: sivuluettelot on lajiteltu suosion mukaan. Pidä osoitin sivun nimen "
"päällä nähdäksesi osumien lukumäärän."

#: ../../stattrans.pl:634
msgid "Outdated translations"
msgstr "Vanhetuneet käännökset"

#: ../../stattrans.pl:636 ../../stattrans.pl:690
msgid "File"
msgstr "Tiedosto"

#: ../../stattrans.pl:638
msgid "Diff"
msgstr "Muutokset"

#: ../../stattrans.pl:640
msgid "Comment"
msgstr "Kommentti"

#: ../../stattrans.pl:641
msgid "Diffstat"
msgstr "Muutostilastot"

#: ../../stattrans.pl:642
msgid "Git command line"
msgstr ""

#: ../../stattrans.pl:644
msgid "Log"
msgstr "Loki"

#: ../../stattrans.pl:645
msgid "Translation"
msgstr "Käännös"

#: ../../stattrans.pl:646
msgid "Maintainer"
msgstr "Ylläpitäjä"

#: ../../stattrans.pl:648
msgid "Status"
msgstr "Tila"

#: ../../stattrans.pl:649
msgid "Translator"
msgstr "Kääntäjä"

#: ../../stattrans.pl:650
msgid "Date"
msgstr "Päiväys"

#: ../../stattrans.pl:657
msgid "General pages not translated"
msgstr "Kääntämättömät yleissivut"

#: ../../stattrans.pl:658
msgid "Untranslated general pages"
msgstr "Kääntämättömät yleissivut"

#: ../../stattrans.pl:663
msgid "News items not translated"
msgstr "Kääntämättömät uutiset"

#: ../../stattrans.pl:664
msgid "Untranslated news items"
msgstr "Kääntämättömät uutiset"

#: ../../stattrans.pl:669
msgid "Consultant/user pages not translated"
msgstr "Kääntämättömät konsultti/käyttäjäsivut"

#: ../../stattrans.pl:670
msgid "Untranslated consultant/user pages"
msgstr "Kääntämättömät konsultti/käyttäjäsivut"

#: ../../stattrans.pl:675
msgid "International pages not translated"
msgstr "Kääntämättömät kansainväliset sivut"

#: ../../stattrans.pl:676
msgid "Untranslated international pages"
msgstr "Kääntämättömät kansainväliset sivut"

#: ../../stattrans.pl:681
msgid "Translated pages (up-to-date)"
msgstr "Käännetyt sivut (ajan tasalla)"

#: ../../stattrans.pl:688 ../../stattrans.pl:838
msgid "Translated templates (PO files)"
msgstr "Käännetyt mallinteet (PO-tiedostot)"

#: ../../stattrans.pl:689 ../../stattrans.pl:841
msgid "PO Translation Statistics"
msgstr "PO-käännöstilastot"

#: ../../stattrans.pl:692 ../../stattrans.pl:855
msgid "Fuzzy"
msgstr "Sumea"

#: ../../stattrans.pl:693
msgid "Untranslated"
msgstr "Kääntämättä"

#: ../../stattrans.pl:694
msgid "Total"
msgstr "Yhteensä"

#: ../../stattrans.pl:711
msgid "Total:"
msgstr "Yhteensä:"

#: ../../stattrans.pl:745
msgid "Translated web pages"
msgstr "Käännetyt www-sivut"

#: ../../stattrans.pl:748
msgid "Translation Statistics by Page Count"
msgstr "Käännöstilastot sivumäärän mukaan"

#: ../../stattrans.pl:763 ../../stattrans.pl:809 ../../stattrans.pl:853
msgid "Language"
msgstr "Kieli"

#: ../../stattrans.pl:764 ../../stattrans.pl:810
msgid "Translations"
msgstr "Käännökset"

#: ../../stattrans.pl:791
msgid "Translated web pages (by size)"
msgstr "Käännetyt www-sivut (koon mukaan)"

#: ../../stattrans.pl:794
msgid "Translation Statistics by Page Size"
msgstr "Käännöstilastot sivukoon mukaan"

#~ msgid "Created with"
#~ msgstr "Luotu työkalulla"

#~ msgid "Origin"
#~ msgstr "Alkuperäinen"

#, fuzzy
#~| msgid "Colored diff"
#~ msgid "Commit diff"
#~ msgstr "Väritetyt muutokset"

#~ msgid "Colored diff"
#~ msgstr "Väritetyt muutokset"

#~ msgid "Unified diff"
#~ msgstr "Unifoidut muutokset"
