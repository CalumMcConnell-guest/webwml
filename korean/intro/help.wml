#use wml::debian::template title="어떻게 데비안을 도울까?"
#use wml::debian::translation-check translation="e42e160ab216bcca146be3acec084557e701ceec" maintainer="Sebul"

<p>데비안의 개발에 도움을 생각하고 있다면
경험자와 비경험자 모두 도움을 줄 영역이 많습니다:</p>

# TBD - Describe requirements per task?
# such as: knowledge of english, experience in area X, etc..

<h3>코딩</h3>

<ul>
<li>여러분은 많은 경험을 가진 응용프로그램을 패키지하고 데비안에 가치있는 것으로 생각하고 이 패키지에 대한 유지관리자가 될 수 있습니다.
더 많은 정보는 <a href="$(HOME)/devel/">데비안 개발자 코너</a>를 읽어 보세요.
</li>
<li>여러분은
<a href="$(HOME)/security/">보안 이슈</a>를 <a href="https://security-tracker.debian.org/tracker/data/report">추적</a>,
<a href="$(HOME)/security/audit/">발견</a> 하고 <a href="$(DOC)/manuals/developers-reference/pkgs.html#bug-security">바로잡을</a> 
수 있습니다. 여러분은
<a href="https://wiki.debian.org/Hardening">패키지</a>,
<a href="https://wiki.debian.org/Hardening/RepoAndImages">저장소와 이미지</a>
그리고 <a href="https://wiki.debian.org/Hardening/Goals">다른 것</a>을 강화하는데
도움을 줄 수 있습니다.
</li>
<li>데비안 운영체제에서 이미 사용 가능한 응용프로그램, 특히 자주 사용하는 프로그램을, 
수정 사항(패치) 또는 추가 정보를 패키지에 대한 <a
href="https://bugs.debian.org/">버그 추적 시스템</a>에 제공하여
도울 수 있습니다.
유지관리팀의 일원이 되어서 패키지 유지관리에 직접 참여하거나
<a href="https://salsa.debian.org/">Salsa</a>에서 소프트웨어 프로젝트에 가입하여
데비안을 위해 개발되는 소프트웨어 프로젝트에 참여할 수도 있습니다.
</li>
<li>새 포트를 시작하거나 기존 포트에 기여하여 여러분이 경험한 운영체제에 대해 데비안 포팅을 도울 수 있습니다.
더 많은 정보를 위하여 <a href="$(HOME)/ports/">가능한 포트 목록</a>를 보세요.
</li>
<li><a href="https://wiki.debian.org/Services">존재</a>하는 데비안 관련 서비스를 개선하거나, 
공동체가 <a href="https://wiki.debian.org/Services#wishlist">필요</a>로 하는 새로운 서비스를 창조하는 것을 도울 수 있습니다.

</ul>

<h3>테스트</h3>

<ul>
<li>단순히 운영체제와 그 안에 제공되는 프로그램을 테스트하고 알려지지 않은 버그를 <a href="https://bugs.debian.org/">버그 추적 시스템</a>을 써서 보고할 수 있습니다.
설명된 문제를 재현할 수 있다면,
패키지와 관련된 버그를 찾아보고 더 많은 정보를 제공하세요.</li>
</ul>

<h3>사용자 지원</h3>
<ul>
# Translators, link directly to the translated mailing lists and provide
# a link to the IRC channel in the user's language
<li>경험있는 사용자라면
<a href="$(HOME)/support#mail_lists">사용자 메일링 리스트</a>를 통하거나
IRC 채널 <tt>#debian</tt>을 사용하여
다른 사람들을 도울 수 있습니다.
지원 옵션과 가능한 소스에 대해서는
<a href="$(HOME)/support">지원 페이지</a>를 읽으세요.
</li>
</ul>

<h3>번역</h3>
<ul>
# TBD - link to translators mailing lists
# Translators, link directly to your group's pages
<li>번역 프로젝트에
(일반적으로 토론은 <a href="https://lists.debian.org/debian-i18n/">i18n 메일링 리스트</a>를 통해 다룹니다)
참여하여
응용프로그램 또는 데비안 관련 정보(웹 페이지, 문서)를 여러분의 언어로 번역하는 것을 도울 수 있습니다.
여러분의 언어가 거기 없다면 새 국제 그룹을 시작할 수도 있습니다.
더 많은 정보는 <a href="$(HOME)/international/">국제화 페이지</a>를 읽으세요.
</li>
</ul>

<h3>문서화</h3>
<ul>
<li><a href="$(HOME)/doc/ddp">데비안 문서 프로젝트</a>를 통하거나
<a href="https://wiki.debian.org/">데비안 위키</a>에 기여하는 것을 통해
공식 문서를 쓰는 것을 도울 수 있습니다.
</li>
<li><a href="https://debtags.debian.org/">debtags</a> 웹 사이트에 태그하고 분류하여
사용자가 찾으려는 소프트웨어를 좀 더 찾기 쉽게 할 수 있습니다.</li>
</ul>

<h3>이벤트</h3>
<ul>
<li>데비안의 <em>공개</em> 얼굴 개발 및 <a href="$(HOME)/devel/website/">웹사이트</a>에 기여 하거나 전 세계 <a href="$(HOME)/events/">이벤트</a> 조직을 도움으로써 도울 수 있습니다.</li>
<li>데비안에 대해 이야기하고 다른 사람들에게 시연함으로써 스스로 홍보하도록 도와주세요.</li>
<li>정규 모임 및 활동을 통해 <a href="https://wiki.debian.org/LocalGroups">지역 데비안 그룹</a>을 만들거나 구성하는 것을 도와주세요.</li>
<li>연례 <a href="http://debconf.org/">데비안 컨퍼런스</a>,
<a href="https://wiki.debconf.org/wiki/Videoteam">이야기 비디오 녹화</a>,
<a href="https://wiki.debconf.org/wiki/FrontDesk">첨석자 인사</a>,
<a href="https://wiki.debconf.org/wiki/Talkmeister">이야기 전 연설</a>,
특별 이벤트(치즈 와인 파티 같은), 준비, 해제 등을 포함하여 데비안을 도울 수 있습니다.
</li>
<li>
여러분 지역의 연례 <a href="http://debconf.org/">데비안 컨퍼런스</a>, 미니 데브컨프,
<a href="https://wiki.debian.org/DebianDay">데비안 데이 파티</a>,
<a href="https://wiki.debian.org/ReleaseParty">릴리스 파티</a>,
<a href="https://wiki.debian.org/BSP">버그 스쿼시 파티</a>,
<a href="https://wiki.debian.org/Sprints">개발 스프린트</a> 및
세계의 <a href="https://wiki.debian.org/DebianEvents">다른 이벤트</a>를 조직할 수 있습니다.
</li>
</ul>

<h3>기부</h3>
<ul>
<li><a href="$(HOME)/donations">장비 및 서비스 기부</a>를 데비안 프로젝트에 해서
다른 사용자 또는 개발자가 그 이익을 얻게 할 수 있습니다.
<a href="$(HOME)/mirror/">세계 미러</a> 사용자를 위한 미러 검색 및
포터를 위한 <a href="$(HOME)/devel/buildd/">auto-builder 시스템</a>을 사용할 수 있습니다.</li>
</ul>

<h3>데비안 사용!</h3>
<ul>
<li>패키지의 <a href="https://wiki.debian.org/ScreenShots">스크린 샷</a>을 만들고 그것을
 <a href="https://screenshots.debian.net/">screenshots.debian.net</a>에 <a href="https://screenshots.debian.net/upload">업로드</a>해서
우리의 사용자가 그것을 써 보기 전에 데비안에서 무슨 소프트웨어가 좋아 보이는지 보게 할 수 있습니다.
</li>
<li><a href="https://packages.debian.org/popularity-contest">popularity-contest submissions</a>를 가능하게 해서
무슨 패키지가 대중적이며 모두에게 쓸 만한지 알 수 있게 할 수 있습니다.
</li>
</ul>

<p>프로젝트에 참여하는 길은 많으며
데비안 개발자가 되기 위해 필요한 것은 아주 적습니다.
여러 다른 프로젝트들은 믿을 수 있고 가치있는 기여자에게 소스 코드 트리에 직접 액세스하는 것을 허용하는
메커니즘이 있습니다.
전형적으로,
데비안에 더 많이 참여하려는 사람은 <a href="$(HOME)/devel/join">프로젝트에 가입</a>하겠지만,
반드시 필요한 건 아닙니다.</p>

<h3>기관</h3>

<p>교육, 상업, 비영리 또는 정부 기관은 여러분의 자원을 이용하여 데비안을 돕는 데 관심있을 겁니다.
여러분의 기관은
<a href="https://www.debian.org/donations">우리에게 기부</a>,
<a href="https://www.debian.org/partners/">우리와 지속적 협력관계 형성</a>,
<a href="https://www.debconf.org/sponsors/">우리 컨퍼런스 후원</a>,
<a href="https://wiki.debian.org/MemberBenefits">데비안 기여자에게 제품 또는 서비스 제공</a>,
<a href="https://wiki.debian.org/ServicesHosting#Outside_the_Debian_infrastructure">데비안 서비스 실험 호스팅 제공</a>,
<a href="https://www.debian.org/mirror/ftpmirror">소프트웨어</a>,
<a href="https://www.debian.org/CD/mirroring/">설치 매체</a>
또는 <a href="https://wiki.debconf.org/wiki/Videoteam/Archive">컨퍼런스 비디오</a> 미러 실행
또는 
<a href="https://www.debian.org/users/">평가 제공</a>
또는 데비안 <a href="https://www.debian.org/events/merchandise">상품</a>팔기,
<a href="https://www.debian.org/CD/vendors/">설치 매체</a>,
<a href="https://www.debian.org/distrib/pre-installed">미리설치된 시스템</a>,
<a href="https://www.debian.org/consultants/">컨설팅</a> 또는
<a href="https://wiki.debian.org/DebianHosting">호스팅</a> 등을 통해 소프트웨어 커뮤니티를 증진할 수 있습니다.
</p>

<p>기관에서 운영체제를 사용하고, 데비안 운영체제 및 커뮤니티에 대해 가르치고, 근무 시간 동안 기여하도록 지시하거나 
<a href="$(HOME)/events/">이벤트</a>로 보내서 직원이 데비안에 참여하도록 장려할 수 있습니다.
</p>
