#use wml::debian::cdimage title="데비안 CD 이미지를 jigdo로 다운로드" BARETITLE=true
#use wml::debian::toc
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::installer
#include "$(ENGLISHDIR)/releases/images.data"
#include "$(ENGLISHDIR)/devel/debian-installer/images.data"
#use wml::debian::translation-check translation="bfe74b043e2ffbc3a218652daf006b9ae50adde9" maintainer="Sebul"

<p>Jigsaw Download, 또는 짧게 <a href="http://atterer.org/jigdo/">jigdo</a>는
데비안 CD/DVD 이미지를 대역폭 친화적 방법입니다.</p>

<toc-display/>

<toc-add-entry name="why">왜 jigdo가 직접 다운로드하는 것 보다 나은가?
</toc-add-entry>

<p>왜냐면 빠르기 때문!
여러가지 이유로, CD/DVD 이미지를 위한 미러는 "보통" 데비안 아카이브보다 적습니다.
결과적으로, CD 이미지 미러에서 다운로드한다면, 그 미러는 여러분과 멀리 떨어져 있을
뿐 아니라, 특히 릴리스 후에 오버로드 됩니다.
</p>

<p>게다가, 어떤 형식의 이미지는 전체 <tt>.iso</tt> 다운로드가 불가능한데, 왜냐면
서버가 그것을 호스트할 충분한 공간이 안 되기 때문입니다.
</p>

<p>물론, "보통" 데비안 미러는 CD/DVD 이미지를 갖지 않으므로,
jigdo는 어떻게 그것을 거기서 다운로드할 수 있을까요? jigdo 아카이브는 CD/DVD에 있는 모든 파일을
개별적으로 다운로드해 저장합니다.
다음 단계는, 이 모든 파일은 CD/DVD의 정확한 사본 큰 파일 하나로 조립됩니다. 
그러나, 이 모든 것은 장면 뒤에서 발생합니다 - <em>여러분</em>이 필요한 모든 것은
"<tt>.jigdo</tt>" 파일의 위치 도구를 다운로드해 처리하는 겁니다.
</p>

<p>더 자세한 정보는
<a href="https://www.einval.com/~steve/software/jigdo/">jigdo 홈페이지</a>에 있습니다. 
jigdo 개발을 도울 자원봉사자는 언제나 환영합니다!
</p>

<toc-add-entry name="how">jigdo로 이미지를 다운로드하는 방법</toc-add-entry>

<ul>

  <li><tt>jigdo-lite</tt>를 포함한 패키지를 받으세요, 이것은
<a href="https://www.einval.com/~steve/software/jigdo/">jigdo 홈페이지</a>에서
GNU/리눅스, 윈도우 및 솔라리스 용으로 가능합니다.
FreeBSD를 위해서는,
/usr/ports/net-p2p/jigdo에서 설치하거나
<tt>pkg_add -r jigdo</tt> 명령으로 패키지를 가져옵니다.
  </li>

  <li><tt>jigdo-lite</tt> 스크립트를 돌립니다.
처리할 "<tt>.jigdo</tt>" 파일의 URL을 물을 겁니다.
(원하면 명령행에서 여러분이 URL을 제공할 수도 있습니다.)
</li>

  <li><a href="#which">아래</a>에 나열된 위치 중 하나에서,
다운로드할 "<tt>.jigdo</tt>" 파일을 고르고, 
<tt>jigdo-lite</tt> 프롬프트에서 URL을 치세요.
각 "<tt>.jigdo</tt>" 파일은 한 "<tt>.iso</tt>" CD/DVD 이미지에 해당합니다.
</li>

  <li>처음 사용자라면, "Files to scan" 프롬프트에서 그냥 엔터 칩니다.
</li>

  <li>"Debian mirror" 프롬프트에서, 
  <kbd>http://deb.debian.org/debian/</kbd> 또는
  <kbd>http://ftp.<strong><var>XY</var></strong>.debian.org/debian/</kbd> 치는데, 여기서
  <strong><var>XY</var></strong>는 여러분의 나라를 위한 2글자 코드
(예를 들어, <tt>us</tt>, <tt>de</tt>, <tt>uk</tt>입니다.
<a href="$(HOME)/mirror/list">가능한 목록은 ftp.<var>XY</var>.debian.org locations</a>.)

  <li>스크립트가 출력한 명령을 따르세요.
모든 것이 잘 되면, 스크립트는 생생된 이미지의 체크섬을 계산하고
체크섬이 원본 이미지와 맞음을 알려줄 겁니다.
</li>

</ul>

<p>이 절차에 대한 자세한, 단계적 설명을 보려면
<a href="http://www.tldp.org/HOWTO/Debian-Jigdo/">Debian jigdo mini-HOWTO</a>를
보세요.
이 HOWTO는 또 jigdo의 개선된 기능, 예를 들어 오래된 버전의 CD 이미지를 현재 버전
(바뀐것만 다운로드해, 새 이미지 전체가 아니라)으로 업그레이드 하는 것을 설명합니다.
</p>

<p>이미지를 다운로드해 CD에 쓴 다음에는,
<a
href="$(HOME)/releases/stable/installmanual">설치 절차 자세한 정보</a>를 보세요.
</p>

<toc-add-entry name="which">공식 이미지</toc-add-entry>

<h3><q>안정</q> 릴리스를 위한 공식 jigdo 파일</h3>

<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <stable-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <stable-full-dvd-jigdo />
</div>
<div class="clear"></div>
</div>
<div class="line">
<div class="item col50">
<p><strong>Blu-ray</strong></p>
  <stable-full-bluray-jigdo />
</div>
</div>

<p>설치 전에 문서를 보세요.
설치 전에 <strong>한 문서만 읽어야 한다면</strong>,
<a href="$(HOME)/releases/stable/i386/apa">설치 하우투</a>,간단한 설치 절차를 읽으세요.
다른 쓸모 있는 문서:
</p>
<ul>
<li><a href="$(HOME)/releases/stable/installmanual">설치 지침</a>,
    자세한 설치 명령</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian-Installer
    문서</a>, FAQ 및 공통문답 포함</li>
<li><a href="$(HOME)/releases/stable/debian-installer/#errata">Debian-Installer
    정오표</a>, 설치관리자 알려진 문제 목록</li>
</ul>

<h3><q>testing</q> 배포용 공식 jigdo 파일</h3>
<div class="line">
<div class="item col50">
<p><strong>CD</strong></p>
  <devel-full-cd-jigdo />
</div>
<div class="item col50 lastcol">
<p><strong>DVD</strong></p>
  <devel-full-dvd-jigdo />
</div>
</div>
<hr />

<toc-add-entry name="search">CD 이미지에서 내용 검색</toc-add-entry>

<p><strong>어떤 파일이 어떤 CD/DVD 이미지에 들어 있나요?</strong>
아래에서 다양한 데비안 CD/DVD 이미지에 포함된 파일 목록을 검색할 수 있습니다.
여러 단어를 입력할 수 있으며 각 단어는 파일 이름의 하위 문자열과 일치해야합니다.
예를 들어 "_i386"을 추가하여 결과를 특정 아키텍처로 제한합니다.
모든 아키텍처에서 동일한 패키지를 보려면 "_all"을 추가하십시오.
</p>

<form method="get" action="https://cdimage-search.debian.org/"><p>
<input type="hidden" name="search_area" value="release">
<input type="hidden" name="type" value="simple">
<input type="text" name="query" size="20" value="">
# Translators: "Search" is translatable
<input type="submit" value="검색"></p></form>

<p><strong>무슨 파일이 어떤 이미지에 들어있나?</strong>
어떤 데비안 CD/DVD가 포함한 <em>모든</em> 파일 목록이 필요하면 
<a
href="https://cdimage.debian.org/cdimage/">cdimage.debian.org</a>에서
이미지의 해당하는<tt>list.gz</tt>파일을 보세요.</p>

<hr>

<toc-add-entry name="faq">잦은 문답</toc-add-entry>

<p><strong>내 프록시에서 jigdo를 만들려면?</strong></p>

<p>파일 <tt>~/.jigdo-lite</tt> (또는
<tt>jigdo-lite-settings.txt</tt> 윈도 버전)을 텍스트 편집기로 읽고 "wgetOpts"로 시작하는 행을 찾으세요.
다음 스위치를 행에 추가할 수 있습니다:</p>

<p><tt>-e ftp_proxy=http://<i>LOCAL-PROXY</i>:<i>PORT</i>/</tt>
<br><tt>-e http_proxy=http://<i>LOCAL-PROXY</i>:<i>PORT</i>/</tt>
<br><tt>--proxy-user=<i>USER</i></tt>
<br><tt>--proxy-passwd=<i>PASSWORD</i></tt></p>

<p>물론, 프록시 서버에 대한 바른 값을 대입하세요.
마지막 두 옵션은 프록시가 암호 인증을 쓸 때만 필요합니다.
wgetOpts 행 마지막 <tt>'</tt> 문자 <em>앞</em>에 스위치를 추가해야 합니다.
모든 옵션은 한 행에 있어야 합니다.</p>

<p>리눅스에서 <tt>ftp_proxy</tt> 및 <tt>http_proxy</tt> 환경 변수를 설정
(예를 들어 <tt>/etc/environment</tt> 또는 <tt>~/.bashrc</tt> 파일에)할 수 있습니다.
</p>

<p><strong>으악! 스크립트 실패 - 내가 받은 모든 것이 헛된 건가요?</strong></p>

<p>물론 이런 일이 생기면 안 되지만, 여러 이유로 큰 "<tt>.iso.tmp</tt>"이 만들어지고
<tt>jigdo-lite</tt>가 문제 있는 것처럼 보일 수 있습니다.
이 경우 할 수 있는 몇가지:</p>

<ul>

  <li>Return 키를 눌러 다운로드를 다시 시작하십시오.
시간 초과 또는 기타 일시적 오류로 인해 일부 파일을 다운로드하지 못할 수 있습니다 - 누락된 파일을 다운로드하려고 시도합니다.</li>

  <li>다른 미러를 시도하세요. 어떤 데비안 미러는 동기되지 않았을 수 있습니다 - 다른 미러는 지워진 파일이 남아있거나 업데이트가 미러에 아직 존재하지 않을 수 있습니다.</li>

  <li>이미지에서 빠진 부분을 <tt><a
  href="http://rsync.samba.org/">rsync</a></tt>로 복구 하세요.
먼저, 다운로드 중인 이미지의 바른 rsync URL을 찾으세요:
<a href="../mirroring/rsync-mirrors">안정</a> 또는 <a
  href="../http-ftp/#testing">테스트</a> 이미지에 rsync 접근을 제공하는 서버를 고르고, 바른 경로와 파일 이름을 결정하세요. 디렉터리 목록을 아래 명령으로 얻으세요
  <tt>rsync&nbsp;rsync://cdimage.debian.org/debian-cd/</tt>

  <br>다음에, "<tt>.tmp</tt>" 확장을
  <tt>jigdo-lite</tt>의 임시파일에서 이름을 바꾸고, 원격 URL 및 지역 파일을 동기하세요:
  <tt>rsync&nbsp;rsync://server.org/path/binary-i386-1.iso
  binary-i386-1.iso</tt>

  <br>rsync의 <tt>--verbose</tt> 와
  <tt>--progress</tt> 스위치로 상태 메시지를 얻고, 
  <tt>--block-size=8192</tt>로 스피드를 증가 시키길 수도 있습니다.</li>

  <li>다른 모든 것이 실패해도, 여러분이 다운로드한 데이터를 잃는 건 아닙니다: 
리눅스에서 <tt>.tmp</tt> 파일을 루프-마운트하여 이미 다운로드한 패키지에 접근하고
새 jigdo 파일로부터 이미지를 생성하는데 재사용할 수 있습니다(예 : 다운로드 실패가 테스트 스냅 샷 인 경우 최신 주간 테스트 스냅 샷). 이것을 하려면, 다음 명령을 root로 다운로드가 깨진 디렉터리에서 실행하세요: <tt>mkdir&nbsp;mnt;
  mount&nbsp;-t&nbsp;iso9660&nbsp;-o&nbsp;loop&nbsp;*.tmp&nbsp;mnt</tt>.
다음에, 새 다운로드를 다른 디렉터리에서 시작하고, "Files to scan" 프롬프트에서 <tt>mnt</tt> 디렉터리의 경로를 입력하세요.</li>

</ul>
