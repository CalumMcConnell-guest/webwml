#use wml::debian::translation-check translation="70be9894f7f65be4520c817dfd389d2ee7c87f04" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.
<define-tag description>보안 업데이트</define-tag>
<define-tag moreinfo>
<p>여러 취약점을 빠르고, 스케일러블한 분산 리비전 제어 시스템 git에서 발견했습니다.
</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1348">CVE-2019-1348</a>

    <p>It was reported that the --export-marks option of git fast-import is
    exposed also via the in-stream command feature export-marks=...,
    allowing to overwrite arbitrary paths.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1387">CVE-2019-1387</a>

    <p>It was discovered that submodule names are not validated strictly
    enough, allowing very targeted attacks via remote code execution
    when performing recursive clones.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19604">CVE-2019-19604</a>

    <p>Joern Schneeweisz reported a vulnerability, where a recursive clone
    followed by a submodule update could execute code contained within
    the repository without the user explicitly having asked for that. It
    is now disallowed for `.gitmodules` to have entries that set
    `submodule.&lt;name&gt;.update=!command`.</p></li>

</ul>

<p>In addition this update addresses a number of security issues which are
only an issue if git is operating on an NTFS filesystem (<a href="https://security-tracker.debian.org/tracker/CVE-2019-1349">CVE-2019-1349</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2019-1352">CVE-2019-1352</a> and <a href="https://security-tracker.debian.org/tracker/CVE-2019-1353">CVE-2019-1353</a>).</p>
<p>For the oldstable distribution (stretch), these problems have been fixed
in version 1:2.11.0-3+deb9u5.</p>

<p>For the stable distribution (buster), these problems have been fixed in
version 1:2.20.1-2+deb10u1.</p>

<p>git 패키지를 업그레이드 하는 게 좋습니다.</p>

<p>git의 자세한 보안 상태는 보안 추적 페이지를 참조하십시오:
<a href="https://security-tracker.debian.org/tracker/git">https://security-tracker.debian.org/tracker/git</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4581.data"
