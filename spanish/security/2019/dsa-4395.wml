#use wml::debian::translation-check translation="13c74272ff7923316d1023584e21f19c0a5f1e92"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17481">CVE-2018-17481</a>

    <p>Se descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5754">CVE-2019-5754</a>

    <p>Klzgrad descubrió un error en la implementación de las comunicaciones QUIC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5755">CVE-2019-5755</a>

    <p>Jay Bosamiya descubrió un error de implementación en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5756">CVE-2019-5756</a>

    <p>Se descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5757">CVE-2019-5757</a>

    <p>Alexandru Pitis descubrió un error de confusión de tipo en la implementación del formato
    de imágenes SVG.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5758">CVE-2019-5758</a>

    <p>Zhe Jin descubrió un problema de «uso tras liberar» en blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5759">CVE-2019-5759</a>

    <p>Almog Benin descubrió un problema de «uso tras liberar» al gestionar páginas HTML
    que contienen elementos «select».</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5760">CVE-2019-5760</a>

    <p>Zhe Jin descubrió un problema de «uso tras liberar» en la implementación de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5762">CVE-2019-5762</a>

    <p>Se descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5763">CVE-2019-5763</a>

    <p>Guang Gon descubrió un error de validación de la entrada en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5764">CVE-2019-5764</a>

    <p>Eyal Itkin descubrió un problema de «uso tras liberar» en la implementación de WebRTC.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5765">CVE-2019-5765</a>

    <p>Sergey Toshin descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5766">CVE-2019-5766</a>

    <p>David Erceg descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5767">CVE-2019-5767</a>

     <p>Haoran Lu, Yifan Zhang, Luyi Xing y Xiaojing Liao informaron de un error
     en la interfaz de usuario de WebAPKs.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5768">CVE-2019-5768</a>

    <p>Rob Wu descubrió un error de imposición de reglas en las herramientas para el programador.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5769">CVE-2019-5769</a>

    <p>Guy Eshel descubrió un error de validación de la entrada en blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5770">CVE-2019-5770</a>

    <p>hemidallt descubrió un problema de desbordamiento de memoria en la implementación de WebGL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5772">CVE-2019-5772</a>

    <p>Zhen Zhou descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5773">CVE-2019-5773</a>

    <p>Yongke Wong descubrió un error de validación de la entrada en la implementación
    de IndexDB.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5774">CVE-2019-5774</a>

    <p>Junghwan Kang y Juno Im descubrieron un error de validación de la entrada en la
    implementación de SafeBrowsing.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5775">CVE-2019-5775</a>

    <p>evil1m0 descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5776">CVE-2019-5776</a>

    <p>Lnyas Zhang descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5777">CVE-2019-5777</a>

    <p>Khalil Zhani descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5778">CVE-2019-5778</a>

    <p>David Erceg descubrió un error de imposición de reglas en la implementación
    de Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5779">CVE-2019-5779</a>

    <p>David Erceg descubrió un error de imposición de reglas en la implementación
    de ServiceWorker.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5780">CVE-2019-5780</a>

    <p>Andreas Hegenberg descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5781">CVE-2019-5781</a>

    <p>evil1m0 descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5782">CVE-2019-5782</a>

    <p>Qixun Zhao descubrió un error de implementación en la biblioteca javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5783">CVE-2019-5783</a>

    <p>Shintaro Kobori descubrió un error de validación de la entrada en las herramientas para
    el programador.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5784">CVE-2019-5784</a>

    <p>Lucas Pinheiro descubrió un error de implementación en la biblioteca
    javascript v8.</p></li>

</ul>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 72.0.3626.96-1~deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4395.data"
