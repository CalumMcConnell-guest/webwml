#use wml::debian::template title="Debian buster -- Installation Guide" BARETITLE=true
#use wml::debian::release
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/arches.data"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#use wml::debian::translation-check translation="91954674647a3fceb1158db18ea7d17bc1093ce9"

<if-stable-release release="stretch">
<p>Detta är en <strong>betaversion</strong> av Installationsguiden för Debian
10, kodnamn Buster, som inte är släppt ännu. Informationen som presenteras
här kan vara föråldrad och/eller felaktig på grund av förändringar i
installeraren. Du kan vara intresserad av
<a href="../stretch/installmanual">Installationsguiden för Debian
9, kodnamn Stretch</a>, som är den senaste släppta versionen av
Debian; eller av <a href="https://d-i.debian.org/manual/">Utvecklingsversionen
av Installationsguiden</a>, som är den version som är mest up-to-date
av detta dokument.</p>
</if-stable-release>

<p>Installationsinstruktioner, tillsammans med nedladdningsbara filer, finns
tillgängliga för var och en av arkitekturerna som stöds:</p>

<ul>
<:= &permute_as_list('', 'Installation Guide'); :>
</ul>

<p>Om du har ställt in din webbläsares språkanpassning kommer länkarna ovan
att automatiskt hämta rätt HTML-version &mdash; se 
<a href="$(HOME)/intro/cn">innehållsförhandling</a>.
Annars, välj den kombination av arkitektur, språk och format du vill ha från
tabellen nedan.</p>

<div class="centerdiv">
<table class="reltable">
<tr>
  <th align="left"><strong>Architecture</strong></th>
  <th align="left"><strong>Format</strong></th>
  <th align="left"><strong>Languages</strong></th>
</tr>
<: &permute_as_matrix_new( file => 'install', langs => \%langsinstall,
			   formats => \%formats, arches => \@arches,
			   html_file => 'index', namingscheme => sub {
			   $_[2] eq html ? "$_[0].$_[1].$_[2]" : "$_[0].$_[2].$_[1]" } ); :>
</table>
</div>
