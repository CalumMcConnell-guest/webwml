#use wml::debian::template title="Razões para escolher o Debian" MAINPAGE="true"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672"

<p>Existem muitas razões para que os(as) usuários(as) escolham o Debian como
seu sistema operacional.</p>

<h1>Motivos principais</h1>

# primeiro tente listar os motivos para usuários(as) finais

<dl>
  <dt><strong>Debian é software livre.</strong></dt>
  <dd>
    O Debian é feito de software livre e de código aberto e sempre será 100%
    <a href="free">livre</a>. Livre para qualquer pessoa usar, modificar e
    distribuir. Esta é nossa principal promessa para
    <a href="../users">nossos(as) usuários(as)</a>. Ele também é grátis (o
    termo "free" em inglês pode tanto significar "livre" como "grátis", por
    isso enfatizamos a distinção).
  </dd>
</dl>

<dl>
  <dt><strong>Debian é um sistema operacional baseado em Linux, estável e seguro.</strong></dt>
  <dd>
    O Debian é um sistema operacional para uma ampla variedade de dispositivos,
    incluindo laptops, desktops e servidores. Usuários(as) apreciam sua
    estabilidade e confiabilidade desde 1993. Fornecemos configurações
    padronizadas coerentes para cada pacote. Os(As) desenvolvedores(as) Debian
    fornecem atualizações de segurança para todos os pacotes durante seus
    ciclos de vida sempre que possível.
  </dd>
</dl>

<dl>
  <dt><strong>Debian tem um vasto suporte a hardware.</strong></dt>
  <dd>
    A maior parte do hardware já é suportado pelo kernel Linux. Drivers
    proprietários para hardware estão disponíveis quando os softwares livres
    não são suficientes.
  </dd>
</dl>

<dl>
  <dt><strong>Debian fornece atualizações suaves.</strong></dt>
  <dd>
    O Debian é bem conhecido por suas atualizações fáceis e suaves dentro de um
    ciclo de versão, e também para a próxima versão principal.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian é a semente e a base para muitas outras distribuições.</strong></dt>
  <dd>
    Muitas das mais populares distribuições Linux, como Ubuntu, Knoppix, PureOS,
    SteamOS ou Tails, escolhem o Debian como base para seus softwares.
    O Debian fornece todas as ferramentas para que qualquer pessoa possa
    estender os pacotes de software, a partir do repositório Debian, com seus
    próprios pacotes para se ajustar às suas necessidades.
  </dd>
</dl>

<dl>
  <dt><strong>O projeto Debian é uma comunidade.</strong></dt>
  <dd>
    O Debian não é somente um sistema operacional. O software é coproduzido por
    centenas de voluntários(as) ao redor do planeta. Você pode ser parte da
    comunidade Debian até mesmo se não for programador(a) ou administrador(a)
    de sistemas. O Debian é orientado pela comunidade e por consensos, e tem
    uma <a href="../devel/constitution">estrutura de governança democrática</a>.
    Como todos(as) os(as) desenvolvedores(as) Debian possuem direitos iguais,
    ele não pode ser controlado por uma única empresa. Temos
    desenvolvedores(as) em mais de 60 países e suportamos traduções para nosso
    instalador Debian em mais de 80 idiomas.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian tem múltiplas opções de instalador.</strong></dt>
  <dd>
    Usuários(as) finais usarão nosso
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">CD Live</a>
    incluindo o instalador de fácil utilização Calamares, necessitando de
    muito pouco conhecimento prévio. Usuários(as) mais experientes podem usar
    nosso instalador completo, enquanto especialistas podem refinar a
    instalação ou até mesmo usar uma ferramenta de instalação automática via
    rede.
  </dd>
</dl>

<br>

<h1>Ambiente empresarial</h1>

<p>
  Se você precisa do Debian em um ambiente profissional, talvez aprecie esses
  benefícios adicionais:
</p>

<dl>
  <dt><strong>O Debian é confiável.</strong></dt>
  <dd>
    O Debian confirma sua confiabilidade a cada dia em milhares de cenários
    reais, desde um laptop de usuário(a) até aceleradores de partículas,
    passando pelo mercado financeiro e pela indústria automotiva. Também é
    popular no mundo acadêmico, na ciência e no setor público.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian tem muitos(as) especialistas.</strong></dt>
  <dd>
    Nossos(as) mantenedores(as) de pacotes não apenas tomam conta do
    empacotamento e incorporação de novas versões de aplicativos ao Debian.
    Frequentemente eles(as) são especialistas nos softwares originais e
    contribuem diretamente para o desenvolvimento desses softwares. Às vezes,
    também fazem parte desses softwares.
  </dd>
</dl>

<dl>
  <dt><strong>O Debian é seguro.</strong></dt>
  <dd>
    O Debian tem suporte à segurança para suas versões estáveis (stable). Muitas
    outras distribuições e pesquisadores(as) de segurança confiam no rastreador
    de segurança do Debian.
  </dd>
</dl>

<dl>
  <dt><strong>Suporte de longo prazo.</strong></dt>
  <dd>
    Existe um <a href="https://wiki.debian.org/LTS">suporte de longo prazo</a>
    (LTS - Long Term Support) sem custos. Ele oferece suporte estendido
    para a versão estável (stable) por 5 anos ou mais. Há também a
    iniciativa
    <a href="https://wiki.debian.org/LTS/Extended">LTS Estendido</a> (Extended
    LTS) que aumenta o suporte para um conjunto limitado de pacotes para além
    de 5 anos.
  </dd>
</dl>

<dl>
  <dt><strong>Imagens para computação em nuvem.</strong></dt>
  <dd>
    Imagens oficiais para computação em nuvem estão disponíveis para todas as
    principais plataformas de nuvem. Nós também fornecemos as ferramentas e
    configurações para que você possa construir sua própria imagem personalizada
    para computação em nuvem. Você também pode usar o Debian em máquinas
    virtuais em um desktop ou em um contêiner.
  </dd>
</dl>

<br>

<h1>Desenvolvedores(as)</h1>
<p>O Debian é amplamente utilizado por todo tipo de desenvolvedor(a) de
   software e hardware.</p>

<dl>
  <dt><strong>Sistema de rastreamento de bugs publicamente disponível.</strong></dt>
  <dd>
    Nosso <a href="../Bugs">sistema de rastreamento de bugs</a> (BTS - Bug 
    Tracking System) está publicamente disponível para qualquer pessoa via
    navegador web. Não escondemos nossos bugs de software e você pode
    facilmente enviar relatórios de bug.
  </dd>
</dl>

<dl>
  <dt><strong>Internet das Coisas e dispositivos embarcados.</strong></dt>
  <dd>
    Nós suportamos uma ampla variedade de dispositivos como Raspberry Pi,
    variantes do QNAP, dispositivos móveis, roteadores domésticos e vários
    computadores de placa única (Single Board Computers - SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Múltiplas arquiteturas de hardware.</strong></dt>
  <dd>
    Suporte para uma <a href="../ports">longa lista</a> de arquiteturas de CPU,
    incluindo amd64, i386, múltiplas versões de ARM e MIPS, POWER7, POWER8,
    IBM System z, RISC-V. O Debian também está disponível para arquiteturas de
    nicho, mais antigas e específicas.
  </dd>
</dl>

<dl>
  <dt><strong>Disponibilidade de vasta quantidade de pacotes de software.</strong></dt>
  <dd>
    O Debian tem a maior quantidade de pacotes instalados disponível
    (atualmente <packages_in_stable>). Nossos pacotes usam o formato deb, que é
    bem conhecido por sua elevada qualidade.
  </dd>
</dl>

<dl>
  <dt><strong>Diferentes escolhas de versões.</strong></dt>
  <dd>
    Além de nossa versão estável (stable), você obtém as mais novas versões de
    software ao usar as versões teste (testing) e instável (unstable).
  </dd>
</dl>

<dl>
  <dt><strong>Qualidade superior com a ajuda de ferramentas e políticas para desenvolvedores(as).</strong></dt>
  <dd>
    Várias ferramentas para desenvolvedores(as) auxiliam a manter a qualidade
    em um alto patamar, e nossas <a href="../doc/debian-policy/">políticas</a>
    definem os requerimentos técnicos que cada pacote deve atender para ser
    incluído na distribuição. Nossa integração contínua executa o software
    autopkgtest, piuparts é nossa ferramenta de testes de instalação,
    atualização e remoção, e lintian é um compreensivo verificador de pacotes
    para pacotes Debian.
  </dd>
</dl>

<br>

<h1>O que nossos(as) usuários(as) dizem</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      Para mim, é o nível perfeito de facilidade de uso e estabilidade. Eu já
      usei várias distribuições durante esses anos, mas o Debian é a única que
      simplesmente funciona.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Totalmente estável. Milhares de pacotes. Excelente comunidade.
    </strong></q>
  </li>

  <li>
    <q><strong>
      O Debian, para mim, é o símbolo da estabilidade e da facilidade de uso.
    </strong></q>
  </li>
</ul>

