#use wml::debian::template title="Site web do Debian em diferentes idiomas" BARETITLE=true
#use wml::debian::toc
#use wml::debian::translation-check translation="c646e774c01abc2ee3d32c65d6920ea4f03159dc"

<protect pass=2>
<:
$lang = languages_footer();
$lang =~ s/<div id="/<div class ="/g;
print $lang;
:>
</protect>

<define-tag toc-title-formatting endtag="required">%body</define-tag>
<define-tag toc-item-formatting endtag="required">[%body]</define-tag>
<define-tag toc-display-begin><p></define-tag>
<define-tag toc-display-end></p></define-tag>

<p>Obviamente, nem todo mundo fala o mesmo idioma. Enquanto a web
cresce, torna-se mais comum encontrar páginas que estão disponíveis
em múltiplos idiomas. Um padrão foi então introduzido, chamado
<a href="$(HOME)/devel/website/content_negotiation">negociação de conteúdo</a>,
que permite a uma pessoa definir o(s) idioma(s)
na qual prefere receber os documentos. A versão atual entregue é
negociada entre seu navegador e o servidor; seu navegador envia
suas preferências e o servidor decide que versão enviar baseado nas
suas preferências e nas versões disponíveis do documento.</p>

<p>Note que selecionando um idioma diferente (a partir da lista de traduções
listada no rodapé da página) você verá apenas a página atual naquele idioma.
Isto <em>não</em> muda o idioma padrão. Se você clicar em um link para uma
página diferente, ela será exibida no idioma original novamente. Para mudar
seu idioma <em>padrão</em>, você precisa mudar suas preferências de idioma
na configuração do seu navegador.
Se isso não for possível, você pode substituir as preferências de idioma do navegador.
Essas opções são explicadas abaixo</p>

<p>Você também pode adicionar informação sobre preferências de idioma nesta
<a href="http://www.w3.org/International/questions/qa-lang-priorities">página W3C</a>.</p>

<ul>
<li><a href="#fix">O que fazer se uma página do Debian está no idioma errado</a></li>
<li><a href="#override">Como substituir a configuração do idioma</a></li>
<li><a href="#howtoset">Como definir a configuração do idioma</a></li>
<li>Onde alterar as configurações para os seguintes navegadores:
  <toc-display /></li>
</ul>

<hr />

<h3><a name="fix">O que fazer se uma página do Debian está no idioma errado</a></h3>

<p>A primeira e mais comum razão para um documento ser recebido
no idioma errado de um servidor web Debian é um navegador mal
configurado. Por favor veja a seção sobre <a href="#howtoset">como
definir o idioma preferido</a> para saber como consertar isso.</p>

<p>A segunda razão são os caches quebrados ou mal configurados. Esse
é um problema crescente quanto mais os ISPs veem o caching como uma
maneira de diminuir o tráfico de rede. Leia a
<a href="#cache">nota sobre servidores web com cache</a> mesmo se você
não acha que usa um.</p>

<p>A terceira razão é haver um problema com o
<a href="https://www.debian.org/">www.debian.org</a>.
Apenas poucos problemas com o recebimento de idioma errado relatados a
nós nos últimos anos foram causados por um problema no nosso lado. Nós,
por isso, sugerimos que você investigue bem as duas principais fontes de
problemas antes de contatar-nos. Se você descobrir que o
<a href="https://www.debian.org/">https://www.debian.org/</a>
está funcionando mas um dos espelhos não, relate isso a nós e nós
contataremos os(as) mantenedores(as) do repositório.</p>

<p>Depois de consertar qualquer um desses problemas, sugerimos que você
limpe o cache local (de disco e memória) no seu navegador antes de tentar
visualizar as páginas de novo. Também sugerimos que você use o
<a href="https://packages.debian.org/stable/web/lynx">lynx</a>
ao testar. Ele é o único navegador que sabemos que segue 100% as
especificações HTTP de negociação de conteúdo.</p>

<p>
É melhor você configurar seu idioma nas preferências do seu navegador,
mas como último recurso você pode <a href="#override">substituir o idioma
preferido</a>.
</p>

<h3><a name="cache">Problemas em potencial com servidores de proxy</a></h3>

<p>Servidores proxy são, essencialmente, servidores web que não têm conteúdo
próprio. Eles ficam entre os(as) usuários(as) e os servidores web reais. Eles
pegam suas requisições a páginas web e buscam a página. Depois disso, eles
repassam a página a você mas também fazem uma cópia local, que fica no cache,
para requisições posteriores. Isso pode realmente baixar o tráfego de rede
quando muitos(as) usuários(as) requisitam a mesma página.</p>

<p>Isso é uma grande ideia na maior parte do tempo, mas falha quando o cache está errado.
Em particular, alguns servidores de proxy antigos não entendem negociação de conteúdo.
Isso resulta em caches de uma página em um idioma e o provimento dela, mesmo se
um idioma diferente for solicitado depois. A única solução é atualizar ou substituir
o software de cache.</p>

<p>Historicamente, as pessoas apenas usavam um proxy quando configuravam
seus navegadores para passar por um. Esse não é mais o caso. Seu ISP pode
redirecionar todas as requisições HTTP através de um proxy transparente. Se
o proxy não lida com negociação de conteúdo apropriadamente, então os(as)
usuários(as) podem receber páginas armazenadas no idioma errado. O único jeito
de consertar isso é reclamar com seu ISP para que eles atualizem ou troquem
o software dele.</p>

<hr />

<h2><a name="override">Como substituir a configuração do idioma</a></h2>

<p>
Se você não consegue configurar <a href="#howtoset">seu idioma preferido</a>
em seu navegador, dispositivo ou ambiente,
então você pode substituir as preferências do seu navegador
utilizando os botões de idioma abaixo.
Isso  definirá um idioma único como
preferível aos idiomas que o navegador informa como suas preferências.
</p>

<p>
Por favor, observe que isto gravará um
<a href="https://en.wikipedia.org/wiki/HTTP_cookie">cookie</a>
que contém sua seleção de idioma.
Seu navegador apagará o cookie se você não visitar este site por um mês.
Você pode apagar o cookie agora escolhendo a opção "Browser default (Navegador padrão)".
</p>

<protect pass=2>
<: print language_selector_buttons(); :>
</protect>

<h2><a name="howtoset">Como definir a configuração do idioma</a></h2>

<p>Você deve definir o idioma preferido para todos os idiomas que
você fala, ordenados por sua preferência. É uma boa ideia adicionar
o inglês ('en') como um substituto (último da lista) porque o
idioma original das páginas web do Debian é o inglês e pode ser que
nem todos os documentos estejam traduzidos no(s) seu(s) idioma(s)
preferido(s).</p>

# os tradutores podem modificar o exemplo abaixo para mencionar seu idioma

<p>Por exemplo, se você é um falante nativo de português, você
pode querer definir sua variável de idioma para incluir primeiro
o português (com o código de idioma '<code>pt</code>' ), seguido
pelo inglês (com o código de idioma '<code>en</code>').</p>

<p>Veja abaixo para <a href="#setting">instruções exatas sobre
como fazer isso em navegadores específicos</a>.</p>

<p>Como você pode ver aqui, a maioria dos navegadores irá
apresentar-lhe alguma espécie de interface para o(a) usuário(a) que esconde
alguns dos detalhes sobre a definição do idioma preferido. Se este
não for o caso, note uma simplificação importante no parágrafo anterior:
se você está apenas especificando uma lista de idiomas como 'pt, en',
isto ainda não define uma preferência; define opções com o mesmo nível,
e o servidor pode decidir ignorar a sua ordem.  Se você quer especificar
preferências reais, você tem que usar "valores de qualidade" que
são valores decimais entre 0 e 1, onde valores mais altos indicam
uma preferência maior. Assim, no exemplo acima você provavelmente
usaria algo como 'pt; q=1.0, en; q=0.5'.</p>

<p>Uma coisa que você precisa tomar cuidado é usar subcategorias
de idiomas. Usar 'pt-BR, en', por exemplo, não faz o que a maioria
das pessoas esperam (se elas não tiverem lido a especificação do HTTP).</p>

<p><strong>Nós recomendamos fortemente que você não adicione extensões de
países a um idioma a menos que tenha boas razões</strong>. Se você adicionar,
assegure-se de que você também inclua o idioma sem a extensão.</p>

<p>Explicação: um servidor que recebe um pedido para um documento com
o idioma preferido definido como 'pt-BR, en' não irá servir a
versão em português ('pt') antes da versão em inglês ('en').
Ele só servirá a versão em português se houver uma versão do arquivo
com 'pt-br' como extensão de idioma.

<p>Portanto, você deve configurar seu navegador para enviar
'pt-BR, pt, en', ou simplesmente 'pt, en'. Isso funciona numa
situação contrária, no entanto. Por exemplo, um servidor pode
retornar 'pt-br' se 'pt' for requisitado.</p>

<p>Para maiores informações sobre como configurar o idioma preferido, veja
<a href="https://httpd.apache.org/docs/current/content-negotiation.html">a documentação
do Apache sobre negociação de conteúdo</a>.</p>

<h3><a name="setting">Configurando o idioma preferencial em um navegador</a></h3>

<p>Para definir o idioma padrão em seu navegador você tem de definir uma
variável que é passada para o servidor de web. Como isso é feito depende
do navegador que você está usando.</p>

<dl>

  <dt><strong><toc-add-entry name="chromium">Chrome e Chromium</toc-add-entry></strong></dt>
  <dd>
  <pre>Configurações -&gt; Avançado -&gt; Idiomas -&gt; Idioma</pre>
  </dd>

  <dt><strong><toc-add-entry name="elinks">ELinks</toc-add-entry></strong></dt>
  <dd>Você pode configurar o idioma da interface com:
  <pre>
    Configurar -&gt; Idioma
  </pre>
  Esta opção também altera o idioma dos web sites.
  Você pode alterar este comportamento e fazer ajustes de idioma na
variável HTTP em:
  <pre>
    Setup -&gt; Gerenciamento de Opções -&gt; Protocolos -&gt; HTTP
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="epiphany">Epiphany</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Editar -&gt; Preferências -&gt; Idioma -&gt; Idiomas
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="mozillafirefox">Firefox</toc-add-entry></strong></dt>
  <dd>
  Versão 3.0 ou superior:<br />
  Linux:
  <pre>
     Editar -&gt; Preferências -&gt; Idioma e Aparência -&gt; Idioma -&gt; Selecionar...
  </pre>
  Windows:
  <pre>
     Ferramentas -&gt; Opções -&gt; Conteúdo -&gt; Idiomas -&gt; Selecionar...
  </pre>
  Mac OS:
  <pre>
     Firefox -&gt; Preferências -&gt; Conteúdo -&gt; Idiomas -&gt; Selecionar...
  </pre>

  <br />
  Versão 1.5 ou superior:<br />
  Linux:
  <pre>
     Editar -&gt; Preferências -&gt; Avançado -&gt; Geral -&gt; Editar Idiomas
  </pre>
  Windows:
  <pre>
     Ferramentas -&gt; Opções -&gt; Avançado -&gt; Geral -&gt; Editar Idiomas
  </pre>

  <br />
  Versão 0.9 ou superior:<br />
  Linux:
  <pre>
     Editar -&gt; Preferências -&gt; Geral -&gt; Idiomas
  </pre>
  Windows:
  <pre>
     Ferramentas -&gt; Opções -&gt; Geral -&gt; Idiomas
  </pre>

  Em versões mais antigas você pode ir para <kbd>sobre:configurar</kbd> e alterar
  o valor de <kbd>intl.accept_languages</kbd>.
  </dd>

  <dt><strong><toc-add-entry name="galeon">Galeon</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Configuração -&gt; Preferências -&gt; Renderização -&gt; Idiomas
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="ibrowse">IBrowse</toc-add-entry></strong></dt>
  <dd>Vá para Preferências, então Configuração, então Rede. Em "Aceitar
  idioma" ela irá, provavelmente, mostrar "*" por padrão. Se você clicar no
  botão "Localizar", você poderá adicionar seu idioma preferido. Se não, você
  pode escrevê-lo. Depois disso, clique em "OK".
  </dd>

  <dt><strong><toc-add-entry name="icab">iCab</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Edit -&gt; Preferências -&gt; Browser -&gt; Fonts, Idiomas
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="iceweasel">Iceweasel</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Editar -&gt; Preferências -&gt; Conteúdo -&gt; Idiomas -&gt; Escolha
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="ie">Internet Explorer</toc-add-entry></strong></dt>
  <dd>Windows:
  <pre>
     Ferramentas ou Visão ou Extras -&gt; Opções de Internet -&gt; (Geral) Idiomas
  </pre>
  </dd>

  <dd>Mac OS:
  <pre>
     Editar -&gt; Preferências -&gt; Navegador -&gt; Idiomas/Fontes
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="konqueror">Konqueror</toc-add-entry></strong></dt>
  <dd>
  Edite seu arquivo
  <kbd>~/.kde/share/config/kio_httprc</kbd> para incluir a linha
  abaixo:
  <pre>
     Languages=pt;q=1.0, en;q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="lynx">Lynx</toc-add-entry></strong></dt>
  <dd>Você pode ou editar a variável <kbd>preferred_language</kbd> no arquivo
  <kbd>.lynxrc</kbd> ou configurar usando o comando 'O' no lynx.

  <p>Por exemplo, use a seguinte linha em seu arquivo <kbd>.lynxrc</kbd>:</p>

  <pre>
  preferred_language=pt; q=1.0, en; q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="mozilla">Mozilla</toc-add-entry> /
  <toc-add-entry name="netscape">Netscape 4.x</toc-add-entry> e versões
  anteriores</strong></dt>
  <dd>
  <pre>
     Editar -&gt; Preferências -&gt; Navegador -&gt; Idiomas
  </pre>
  Observe: no Netscape 4.x você precisa ter certeza que o idioma selecionado
  é um dos idiomas disponíveis. Algumas pessoas informaram problemas porque
  digitaram o idioma manualmente.
  </dd>

  <dt><strong><toc-add-entry name="netscape3">Netscape 3.x</toc-add-entry></strong></dt>
  <dd>Add
  <pre>
     *httpAcceptLanguage: [texto do Idioma preferido]
  </pre>
  para arquivo padrão do Netscape ou <kbd>~/.Xresources</kbd>
  </dd>

  <dt><strong><toc-add-entry name="opera">Opera</toc-add-entry></strong></dt>
  <dd>Demais versões:
  <pre>
     Arquivo -&gt; Preferências -&gt; Idiomas
  </pre>
  </dd>
  <dd>Linux/*BSD versions 5.x and 6.x:
  <pre>
     Ir para configurações do navegador -&gt; Avançado -&gt; Idiomas -&gt; Idioma
  </pre>
  </dd>
  <dd>Nokia 770 Web Browser:
     Edite o arquivo /home/user/.opera/opera.ini e adicione a seguinte
     linha na seção [Adv User Prefs]:
  <pre>
     HTTP Accept Language=pt;q=1.0,en;q=0.5
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="pie">Pocket Internet Explorer</toc-add-entry></strong></dt>
  <dd>
  # Windows Mobile 2003/2003SE/5.0
  <pre>
     Criar no registro a chave <q>AcceptLanguage</q> em
     HKEY_CURRENT_USER\Software\Microsoft\Internet Explorer\International\\
     com valor <q>fr; q=1.0, en; q=0.5</q> (sem aspas).
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="safari">Safari</toc-add-entry></strong></dt>
  <dd>Safari usa o sistema Mac OS X de preferência, para determinar seu
  idioma preferido:
  <pre>
    Preferências do Sistema -&gt; International -&gt; Idioma
  </pre>
  </dd>

  <dt><strong><toc-add-entry name="voyager">Voyager</toc-add-entry></strong></dt>
  <dd>Vá para Configuração, então Idiomas.  Você pode ou entrar manualmente
  ou clicar em "Obter Local". Clique em "OK" quando terminar.
  </dd>

  <dt><strong><toc-add-entry name="w3">W3</toc-add-entry></strong> (navegador
  baseado no emacs)</dt>
  <dd>
  <pre>(setq url-mime-language-string  "preferred_language=pt; q=1.0, en; q=0.5")</pre>
  ou usar o pacote customizado (assumindo a URL com versão p4.0pre.14):

  <pre>Hypermedia -&gt; URL -&gt; Mime -&gt; Mime Language String...</pre>
  </dd>

  <dt><strong><toc-add-entry name="w3m">W3M</toc-add-entry></strong></dt>
  <dd>
  <pre>
     Opção (o) -&gt; Outros Comportamentos -&gt; Aceitar Idioma
  </pre>
  </dd>

</dl>

<p>Se você tem informações de configuração de um navegador que não está listado acima
por favor, mande para
<a href="mailto:debian-www@lists.debian.org">debian-www@lists.debian.org</a>.</p>
