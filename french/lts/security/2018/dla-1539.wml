#use wml::debian::translation-check translation="ab3be4ee01879fd4484c795bbaa824377c218575" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Samba, un serveur de
fichiers SMB/CIFS, d'impression et de connexion pour Unix.
Le projet « Common vulnérabilités et Exposures » (CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10858">CVE-2018-10858</a>

<p>Svyatoslav Phirsov a découvert qu’une validation insuffisante des entrées
dans libsmbclient permettait à un serveur Samba malveillant d’écrire dans la
mémoire de tas du client.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-10919">CVE-2018-10919</a>

<p>Phillip Kuhrt a découvert que Samba agissant comme contrôleur d’Active Domain
divulguait des attributs sensibles.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 4.2.14+dfsg-0+deb8u10.</p>
<p>Nous vous recommandons de mettre à jour vos paquets samba.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1539.data"
# $Id: $
