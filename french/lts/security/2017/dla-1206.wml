#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Dans LibTIFF 4.0.8, il existe un dépassement de tampon basé sur le tas dans
la fonction t2p_write_pdf dans tools/tiff2pdf.c. Ce dépassement de tas pourrait
conduire à différents dégâts. Par exemple, un document TIFF contrefait peut
conduire à une lecture hors limites dans TIFFCleanup, une libération non valable
dans TIFFClose ou t2p_free, une corruption de mémoire dans
t2p_readwrite_pdf_image ou une double libération de zone de mémoire dans
t2p_free. Étant donné ces possibilités, il pourrait probablement causer
l’exécution de code arbitraire.</p>

<p>Ce débordement est lié à une supposition sous-jacente que toutes les pages du
document tiff auront la même fonction de transfert. Il n’existe rien dans la
norme tiff qui dise que cela soit nécessaire.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 4.0.2-6+deb7u17.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tiff.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1206.data"
# $Id: $
