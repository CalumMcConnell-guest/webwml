#use wml::debian::translation-check translation="16a228d71674819599fa1d0027d1603056286470" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9445">CVE-2016-9445</a> /
<a href="https://security-tracker.debian.org/tracker/CVE-2016-9446">CVE-2016-9446</a>

<p>Chris Evans a découvert que le greffon de GStreamer pour décoder les
fichiers de capture d'écran de VMware permettait l'exécution de code
arbitraire. Il a aussi découvert qu'un tampon initialisé pourrait conduire
à une divulgation de mémoire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9447">CVE-2016-9447</a>

<p>Chris Evans a découvert que le greffon GStreamer 0.10 pour décoder les
fichiers NSF (<q>NES Sound Format</q>) permettait l'exécution de code
arbitraire.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 0.10.23-7.1+deb7u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets gst-plugins-bad0.10.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-712.data"
# $Id: $
