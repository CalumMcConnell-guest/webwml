#use wml::debian::translation-check translation="5c9ff3931d57cce65ef1f582471334f65e51ba79" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Eli Biham et Lior Neumann ont découvert une faiblesse cryptographique dans le
protocole d’appairage Bluetooth LE SC, appelée « Fixed Coordinate Invalid Curve
Attack »
 (<a href="https://security-tracker.debian.org/tracker/CVE-2018-5383">CVE-2018-5383</a>).
Selon les périphériques utilisés, cela pourrait être exploité par un attaquant
proche pour obtenir des informations sensibles, pour un déni de service, ou pour
d’autres impacts de sécurité.</p>

<p>Ce défaut a été corrigé dans le micrologiciel pour Intel Wireless 7260 (B3),
7260 (B5), 7265 (D1) et les adaptateurs 8264, et pour les adaptateurs Qualcomm
Atheros QCA61x4 <q>ROME</q>, version 3.2. D’autres adaptateurs Bluetooth sont
aussi affectés et demeurent vulnérables.</p>

<p>Pour Debian 8 <q>Jessie</q>, ce problème a été corrigé dans
la version 20161130-5~deb8u1.</p>
<p>Nous vous recommandons de mettre à jour vos paquets firmware-nonfree.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1747.data"
# $Id: $
