#use wml::debian::translation-check translation="c4a88a02bda10c85ed10496fa71632fa4b55a426" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans mosquitto, un courtier de message
compatible MQTT version 3.1/3.1.1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7655">CVE-2017-7655</a>

<p>Une vulnérabilité de déréférencement de pointeur NULL dans la bibliothèque
Mosquitto pourrait conduire à des plantages pour ces applications utilisant la
bibliothèque.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12550">CVE-2018-12550</a>

<p>Un fichier d’ACL avec aucune déclaration était traité comme ayant une
politique « allow » par défaut. Le nouveau comportement d’un fichier vide
est une politique d’accès « denied » par défaut (cela est en conformité avec toutes
les nouvelles publications).</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12551">CVE-2018-12551</a>

<p>Des données d’authentification mal formées dans le fichier de mot de passe
pourraient permettre aux clients de contourner l’authentification et obtenir un
accès au courtier.</p>

</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11779">CVE-2019-11779</a>

<p>Correction pour traiter un paquet contrefait SUBSCRIBE contenant un sujet
consistant en approximativement 65400 ou plus caractères « / »
(réglant TOPIC_HIERARCHY_LIMIT à 200)</p>

</li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 1.3.4-2+deb8u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets mosquitto.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1972.data"
# $Id: $
