#use wml::debian::template title="Les acteurs de Debian : qui sommes-nous, que faisons-nous"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="bbb1b67054b9061c9420f1c5826b2fa85f05c738" maintainer="Cyrille Bollu"

# translators: some text is taken from /intro/about.wml

<h2>Développeurs et contributeurs</h2>
<p>Debian est produite par presque un millier de développeurs éparpillés
<a href="$(DEVEL)/developers.loc">autour du monde</a> qui
travaillent bénévolement pendant leur temps libre.
Peu de développeurs se sont en fait rencontrés physiquement.
La communication se fait principalement par courrier électronique
(listes de diffusion sur lists.debian.org) et IRC (canal #debian sur
irc.debian.org).
</p>

<p>La liste complète des membres officiels de Debian peut être consultée sur le
site <a href="https://nm.debian.org/members">nm.debian.org</a>, qui tient à jour
cette liste des membres. Une liste plus importante de contributeurs est disponible
sur <a href="https://contributors.debian.org">contributors.debian.org</a>.</p>

<p>Le projet Debian a une <a href="organization">structure</a> soigneusement
organisée. Pour plus d'informations sur ce à quoi ressemble Debian
vue de l'intérieur, n'hésitez pas à naviguer librement dans le
<a href="$(DEVEL)/">coin des développeurs</a>.</p>

<h3><a name="history">Comment tout cela a-t-il commencé ?</a></h3>

<p>Debian a été lancée en août 1993 par Ian Murdock, comme une
nouvelle distribution qui serait faite de façon ouverte, dans l'esprit
de Linux et de GNU. Debian avait la réputation d'être soigneusement et
consciencieusement mise en place, entretenue et gérée avec
beaucoup de soins. Cela a commencé comme un groupe petit et très soudé
de <i>hackers</i> de logiciel libre, et graduellement cela s'est
développé pour devenir une communauté de développeurs et
d'utilisateurs vaste et bien organisée. Consultez <a
href="$(DOC)/manuals/project-history/">l'histoire détaillée</a>
pour plus de détails.</p>

<p>Comme beaucoup de gens le demandent, Debian se prononce « dé-byanne »
(ou en notation phonétique /&#712;de.bi.&#601;n/).
Cela vient des prénoms du créateur de Debian, Ian Murdock, et de sa femme,
Debra.</p>
  
<h2>Personnes et organisations soutenant Debian</h2>

<p>Beaucoup d'autres personnes et d'organisations font partie de la
communauté Debian :</p>
<ul>
  <li><a href="https://db.debian.org/machines.cgi">hébergement et sponsors matériels</a></li>
  <li><a href="../mirror/sponsors">sponsors des miroirs</a></li>
  <li><a href="../partners/">partenaires <q>développement et services</q></a></li>
  <li><a href="../consultants">consultants</a></li>
  <li><a href="../CD/vendors">vendeurs de support d’installation de Debian</a></li>
  <li><a href="../distrib/preinstalled">vendeurs d'ordinateurs avec Debian préinstallée</a></li>
  <li><a href="../events/merchandise">vendeurs de produits dérivés</a></li>
</ul>

<h2><a name="users">Qui utilise Debian&nbsp;?</a></h2>

<p>Bien qu'aucune statistique précise ne soit disponible (car Debian
ne demande pas aux utilisateurs de s'enregistrer), de nombreux signes
montrent que Debian est utilisée par une grande variété
d'organisations, petites ou grandes, ainsi que par plusieurs milliers
d'individus. Consultez notre page <a href="../users/">Qui utilise
Debian&nbsp;?</a> pour une liste des organisations bien connues qui
ont fourni de courtes descriptions sur comment et pourquoi elles
utilisent Debian.</p>
