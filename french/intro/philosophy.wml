#use wml::debian::template title="Notre philosophie : pourquoi nous le faisons et comment nous le faisons"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="8d625100deb403dba223e3cecbac44a283fe02ff" maintainer="Cyrille Bollu"
# translators: some text is taken from /intro/about.wml

<ul class="toc">
<li><a href="#what">Qu'est-ce que Debian ?</a>
<li><a href="#free">Est-ce que tout est gratuit ?</a>
<li><a href="#how">Comment la communauté fonctionne-t-elle ?</a>
<li><a href="#history">Comment cela a-t-il commencé ?</a>
</ul>

<h2><a name="what">Qu'est-ce que Debian ?</a></h2>

<p>Le <a href="$(HOME)/">Projet Debian</a> est une association d'individus qui
ont pour cause commune de créer un système d'exploitation <a href="free">libre</a>.
Ce système d'exploitation que nous avons créé est appelé <strong>Debian</strong>.</p>

<p>Un système d'exploitation est l'ensemble des programmes et utilitaires de
base qui permettent de faire fonctionner votre ordinateur. Au cœur d'un système
d'exploitation se trouve le noyau. Le noyau est le programme le plus fondamental
d'un ordinateur, il fait tout le travail de base et vous permet de lancer
d'autres programmes.</p>

<p>Les systèmes Debian utilisent actuellement le noyau <a href="https://www.kernel.org/">Linux</a>
ou le noyau <a href="https://www.freebsd.org/">FreeBSD</a>. Linux est un
programme initié par <a href="https://fr.wikipedia.org/wiki/Linus_Torvalds">Linus Torvalds</a>
et développé par des milliers de programmeurs de par le monde. FreeBSD est un
autre système d'exploitation comprenant un noyau et d'autres programmes.</p>

<p>Cependant, un travail est en cours pour fournir d'autres noyaux à Debian,
principalement <a href="https://www.gnu.org/software/hurd/hurd.html">Hurd</a>.
Hurd est une collection de serveurs qui fonctionnent au-dessus d'un micronoyau
(tel que Mach) pour implémenter différentes fonctionnalités. Hurd est un
logiciel libre produit par le <a href="https://www.gnu.org/">projet GNU</a>.</p>

<p>Une grande partie des outils de base qui constituent le système
d'exploitation proviennent du projet <a href="https://www.gnu.org/">GNU</a> ;
d'où les noms : GNU/Linux, GNU/kFreeBSD et GNU/Hurd.
Ces utilitaires sont libres eux aussi.</p>

<p>Bien sûr, ce que les gens veulent ce sont des programmes applicatifs : des
logiciels les aidant à faire ce qu'ils souhaitent faire, depuis l'édition de
documents jusqu'à la gestion d'une entreprise en passant par les jeux et la
réalisation de nouveaux logiciels. Le système Debian est fourni avec plus de
<packages_in_stable> <a href="$(DISTRIB)/packages">paquets</a>
(des logiciels précompilés mis dans un format sympathique pour une installation
facile sur votre machine), un gestionnaire de paquets (APT), et d'autres
utilitaires permettant de gérer des milliers de paquets sur des milliers
d'ordinateurs aussi facilement que d'installer une seule application. Tout cela
est <a href="free">libre</a>.
</p>

<p>C'est un peu comme une tour. À la base, le noyau, au-dessus, les utilitaires
fondamentaux, puis tous les logiciels que vous exécutez sur l'ordinateur et, au
sommet de la tour, Debian qui organise soigneusement l'ensemble afin que tout
puisse fonctionner correctement.

<h2>Est-ce totalement <a href="free" name="free">gratuit ?</a></h2>

<p>Quand nous utilisons le mot « libre » (« free » en anglais), nous nous
référons à la <strong>liberté</strong> du logiciel.
Vous pouvez en lire plus sur
<a href="free">ce que nous entendons par « logiciel libre »</a> et
<a href="https://www.gnu.org/philosophy/free-sw">ce que la Fondation pour le
logiciel
libre (« Free Software Foundation » en anglais) dit</a> à ce sujet.</p>

<p>Vous pourriez vous demander : <q>Pourquoi est que les gens passent des
heures de leur temps libre à réaliser des logiciels, les empaqueter
soigneusement et ensuite les <em>donner ?</em></q> Les réponses sont aussi
variées que les personnes qui contribuent. Certaines personnes aiment aider les
autres. Beaucoup écrivent des programmes pour en savoir plus sur les ordinateurs.
De plus en plus de gens cherchent à éviter l'inflation des prix des logiciels.
Une foule de plus en plus nombreuse contribue en remerciement de tous les
excellents logiciels libres qu'ils ont reçus des autres. Beaucoup de gens du
monde académique créent des logiciels libres pour aider à la diffusion de
l'utilisation des résultats de leurs recherches.
Des entreprises aident à maintenir des logiciels libres de façon à avoir leur
mot à dire sur la façon dont ils sont développés &mdash; il n'y a pas de moyen
plus rapide pour obtenir une nouvelle fonctionnalité que de la développer
soi-même ! Bien sûr, beaucoup d'entre nous trouvent cela très amusant.</p>

<p>Debian est si attachée au logiciel libre que nous avons jugé utile que
notre engagement soit formalisé dans un document écrit. C'est ainsi qu'est né
notre <a href="$(HOME)/social_contract">Contrat Social</a>.</p>

<p>Bien que Debian croit au logiciel libre, dans certains cas les gens veulent
ou ont besoin d'installer des logiciels non libres sur leur machine. Chaque fois
que cela est possible, Debian le premettra. Un nombre croissant de paquets
existent dont l’unique tâche est de permettre d'installer un logiciel non libre
dans un système Debian.</p>

<h2><a name="how">Comment la communauté fonctionne-t-elle ?</a></h2>

<p>Debian est produite par presque un millier de développeurs éparpillés
<a href="$(DEVEL)/developers.loc">autour du monde</a> qui travaillent
bénévolement pendant leur temps libre.
Peu de développeurs se sont en fait rencontrés physiquement.
La communication se fait principalement par courrier électronique (listes de
diffusion sur lists.debian.org) et IRC (canal #debian sur irc.debian.org).
</p>

<p>Le projet Debian a une <a href="organization">structure</a> soigneusement
organisée. Pour plus d'informations sur ce à quoi ressemble Debian vue de
l'intérieur, n'hésitez pas à naviguer librement dans le <a href="$(DEVEL)/">coin
des développeurs</a>.</p>

<p>
Les documents principaux expliquant comment fonctionne la communauté sont les
suivants :
<ul>
<li><a href="$(DEVEL)/constitution">la constitution de Debian</a> ;</li>
<li><a href="../social_contract">le contrat social et les principes du logiciel libre</a> ;</li>
<li><a href="diversity">les principes de diversité et équité</a> ;</li>
<li><a href="../code_of_conduct">le code de conduite</a> ;</li>
<li><a href="../doc/developers-reference/">le guide de référence pour Debian</a> ;</li>
<li><a href="../doc/debian-policy/">la charte Debian</a>.</li>
</ul>

<h2><a name="history">Comment tout cela a-t-il commencé?</a></h2>

<p>Debian a été lancée en août 1993 par Ian Murdock, comme une nouvelle
distribution qui serait faite de manière ouverte, dans l'esprit de Linux et de
GNU. Debian avait la réputation d'être soigneusement et consciencieusement mise
en place, entretenue et gérée avec beaucoup de soins. Cela a commencé comme un
groupe petit et très soudé de <i>hackers</i> de logiciel libre, et graduellement
cela s'est développé pour devenir une communauté de développeurs et
d'utilisateurs vaste et bien organisée. Consultez <a
href="$(DOC)/manuals/project-history/">l'histoire détaillée du projet</a>
pour plus de détails.</p>

<p>Comme beaucoup de gens le demandent, Debian se prononce « dé-byanne »
(ou en notation phonétique /&#712;de.bi.&#601;n/).
Cela vient des prénoms du créateur de Debian, Ian Murdock, et de sa femme,
Debra.</p>
