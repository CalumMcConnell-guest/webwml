msgid ""
msgstr ""
"Project-Id-Version: \n"
"POT-Creation-Date: \n"
"PO-Revision-Date: \n"
"Last-Translator: Med Amine <medeb@protonmail.com>\n"
"Language-Team: \n"
"Language: ar\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"

#: ../../english/index.def:8
msgid "The Universal Operating System"
msgstr "نظام التّشغيل الشّامل"

#: ../../english/index.def:12
msgid "DC19 Group Photo"
msgstr ""

#: ../../english/index.def:15
msgid "DebConf19 Group Photo"
msgstr ""

#: ../../english/index.def:19
msgid "Mini DebConf Hamburg 2018"
msgstr ""

#: ../../english/index.def:22
msgid "Group photo of the MiniDebConf in Hamburg 2018"
msgstr ""

#: ../../english/index.def:26
msgid "Screenshot Calamares Installer"
msgstr ""

#: ../../english/index.def:29
msgid "Screenshot from the Calamares installer"
msgstr ""

#: ../../english/index.def:33 ../../english/index.def:36
msgid "Debian is like a Swiss Army Knife"
msgstr "دبيان يشبه السّكين السّويسري"

#: ../../english/index.def:40
msgid "People have fun with Debian"
msgstr ""

#: ../../english/index.def:43
msgid "Debian people at Debconf18 in Hsinchu really having fun"
msgstr ""

#: ../../english/template/debian/navbar.wml:31
msgid "Bits from Debian"
msgstr "جُذاذات من دبيان"

#: ../../english/template/debian/navbar.wml:31
msgid "Blog"
msgstr "المدوّنة"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews"
msgstr "أخبار هامشية"

#: ../../english/template/debian/navbar.wml:32
msgid "Micronews from Debian"
msgstr "أخبار هامشية من دبيان"

#: ../../english/template/debian/navbar.wml:33
msgid "Planet"
msgstr "الكوكب"

#: ../../english/template/debian/navbar.wml:33
msgid "The Planet of Debian"
msgstr "كوكب دبيان"

#~ msgid "Debian is like a Swiss Knife"
#~ msgstr "دبيان يشبه السّكين السويسري"
